<?php
return [
    'enable' => true,
    'task'   => [
        'listen'            => '127.0.0.1:2345',
        'crontab_table'     => 'tp_crontab', //任务计划表
        'crontab_table_log' => 'tp_crontab_log',//任务计划流水表
        'debug'             => true,
    ],
];
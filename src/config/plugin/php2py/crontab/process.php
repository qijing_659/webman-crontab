<?php

use Php2py\Crontab\Server;

return [
    'cron_task' => [
        'handler' => Server::class,
        'listen'  => 'text://' . config('plugin.php2py.crontab.app.task.listen'), // 这里用了text协议，也可以用frame或其它协议
        'count'   => 1, // 必须是1
    ]
];